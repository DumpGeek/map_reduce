Assignment01
Student name:	Yum Chi Hong
Student ID:	1155121888


Assignment 01 word file directory:
./doc/assignment01(1155121888).docx


Assignment 01 data file directory:
./dat


Assignment 01 source code file directory:
./src

To compile the code by shell:
Please run the shell file with word directory in ./src
Under folder src ...

Problem A:
./run_problem_a.sh

Problem B:
./run_problem_b.sh

Problem C:
./run_problem_c.sh

Problem D:
./run_problem_d.sh

Problem E:
./run_problem_e.sh