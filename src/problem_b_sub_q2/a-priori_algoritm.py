class apriori_algoritm():
	def __init__(self, addr=None, data=None, seperator=" ", minSupport = 1):
		self.addr = addr
		self.data = data
		self.seperator = seperator
		self.dataset = None
		self.minSupport = minSupport

	def load_data_from_file(self):
		with open(self.addr) as a_file:
			self.data = a_file.read()
		self.preprocess_data()

	def preprocess_data(self):
		dataset = []
		for line in self.data.splitlines():
			dataset.append(line.split(self.seperator))
		self.dataset = dataset
		
	def create_c1(self):
		C1 = {}
		for line in self.dataset:
			for item in line:
				if item not in C1:
					C1[item] = 1
				else:
					C1[item] += 1
		return C1
	
	def create_ck(self, k, Lk):
		result_lst = []
		elem_lst = []

		# element extraction
		for elem in Lk:
			for item in elem.split(" "):
				if item not in elem_lst:
					elem_lst.append(item)
		
		# all combination generator
		# for x in Lk:
			# for y in elem_lst:
				# if y not in x.split(" "):
					# new_item = x + " " + y
					# chk_lst = []
					# for item in result_lst:
						# check = [elem in new_item.split(" ") for elem in item.split(" ")]
						# chk_lst.append(all(check))
						# if all(check): break
					# if (not any(chk_lst)) or (result_lst == []):
						# result_lst.append(new_item)
		
		# all combination 2nd version (combination should be generated according to the list)
		set_list = []
		for a_elem in Lk:
			a_set = set()
			for elem in a_elem.split(" "):
				a_set.add(elem)
			for b_elem in Lk:
				b_set = set()
				for elem in b_elem.split(" "):
					b_set.add(elem)
				if a_set != b_set:
					new_set = a_set.union(b_set)
					if len(new_set) == k and new_set not in set_list:
						set_list.append(new_set)	
		for elem in set_list:
			result_lst.append(" ".join(elem))
		
		# count existence
		result_dict = {}
		for line in self.dataset:
			for item in result_lst:
				check = [elem in line for elem in item.split(" ")]
				if all(check):
					if item in result_dict:
						result_dict[item] += 1
					else:
						result_dict[item] = 1
		return result_dict
	
	def scan_data(self, Ck, minSupport):
		result = {}
		for line in self.dataset:
			for elem in Ck:
				if Ck[elem] >= minSupport:
					result[elem] = Ck[elem]
		return result
		
	def run(self):
		if self.dataset == None:
			if self.data != None:
				self.preprocess_data()
			elif self.addr != None:
				self.load_data_from_file()
			else:
				raise ValueError("Missing dataset, please load dataset")
		
		# debug-use: input check
		# print("Input:\n" + (self.data) + "\n")
		
		minSupport = self.minSupport
		k = len(self.dataset)
		
		C, L = None, None
		L_dict = {}
		for i in range(1,k):
			if i == 1:
				C = self.create_c1()
				tmp = self.scan_data(C, minSupport)
			else:
				C = self.create_ck(i, L)
				tmp = self.scan_data(C, minSupport)
			
			# check if L is empty then break, else update L
			if tmp != {}:
				L = tmp
			else:
				break
			
			L_dict.update(L)
			
			# debug-use: parameter C & L check
			print("C: ", repr(C))
			print("L: ", repr(L))
		
		print("Output:")
		print(L_dict)
		print("\n")
		print(",".join(L_dict))
		
		# List of elements contain in the final L
		# elem_in_last_L = " ".join(L).split(" ")
		# debug-use: element in L check
		# print("\nelem_in_last_L\n" + repr(elem_in_last_L) + "\n")
		
		# Filter out the item without containing the elements in final L
		# result_L_dict = L_dict.copy()
		# for item in L_dict:
			# debug-use: item check
			# print("item:\t" + item)
			
			# for elem in item.split(" "):
				# if elem not in elem_in_last_L:
					# del result_L_dict[item]
		
		# print("\nOutput:")
		# for elem in result_L_dict:
			# print(elem + ":\t" + str(result_L_dict[elem]))
	
if __name__ == "__main__":
	# data = "1 3 4\n2 3 5\n1 2 3 5\n2 5"
	# apriori = apriori_algoritm(data = data, minSupport = 2)
	apriori = apriori_algoritm(addr = "../../dat/data_for_problem_b/itemlist10000.txt", minSupport = 100)
	apriori.run()