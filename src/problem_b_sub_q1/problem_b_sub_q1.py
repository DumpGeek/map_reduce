#!/usr/bin/env python
# -*- coding: utf-8 -*

import mincemeat
import glob
import re

__version__ = '0.0.0'	# '{major}.{minor}.{patch}'

# shakespeare text file relative path
text_files = ["../../dat/data_for_problem_b/itemlist10000.txt"]

def file_contents(file_name):
	# modified: specify using utf-8 as encoding to avoid error
	f = open(file_name, encoding="utf8")
	try:
		return f.read()
	finally:
		f.close()

# The data source can be any dictionary-like object

# datasource, cnt = {}, 0
# for line in file_contents(text_files[0]).splitlines():
	# datasource.update({cnt: line})
	# cnt += 1

# datasource, cnt = {}, 0
# a_list = re.compile("(?:^.*$\n?){1,3}",re.M).findall(file_contents(text_files[0]))
# for elem in a_list:
	# datasource[cnt] = elem
	# cnt += 1

#datasource = {1: file_contents(text_files[0])}

data = "1 3 4\n2 3 5\n1 2 3 5\n2 5"
datasource = {None:data}
print(datasource)

def mapfn(k, v):
	p = 1
	s = 2
	minSupport = s/p
	
	def create_c1(dataset):
		C1 = {}
		for line in dataset:
			for item in line:
				if item not in C1:
					C1[item] = 1
				else:
					C1[item] += 1
		return C1
	
	def create_ck(dataset, k, Ck):
		result_lst = []
		elem_lst = []
		
		# element extraction
		for elem in Ck:
			for item in elem.split(" "):
				if item not in elem_lst:
					elem_lst.append(item)
		
		# all combination generator
		# for x in Ck:
			# for y in elem_lst:
				# if y not in x.split(" "):
					# new_item = x + " " + y
					# chk_lst = []
					# for item in result_lst:
						# check = [elem in new_item.split(" ") for elem in item.split(" ")]
						# chk_lst.append(all(check))
						# if all(check): break
					# if (not any(chk_lst)) or (result_lst == []):
						# result_lst.append(new_item)
		
		# all combination 2nd version (combination should be generated according to the list)
		set_list = []
		for a_elem in Lk:
			a_set = set()
			for elem in a_elem.split(" "):
				a_set.add(elem)
			for b_elem in Lk:
				b_set = set()
				for elem in b_elem.split(" "):
					b_set.add(elem)
				if a_set != b_set:
					new_set = a_set.union(b_set)
					if len(new_set) == k and new_set not in set_list:
						set_list.append(new_set)	
		for elem in set_list:
			result_lst.append(" ".join(elem))
		
		# count existence
		result_dict = {}
		for line in self.dataset:
			for item in result_lst:
				check = [elem in line for elem in item.split(" ")]
				if all(check):
					if item in result_dict:
						result_dict[item] += 1
					else:
						result_dict[item] = 1
		return result_dict
	
	def scan_data(dataset, Ck, minSupport):
		result = {}
		for line in dataset:
			for elem in Ck:
				if Ck[elem] >= minSupport:
					result[elem] = Ck[elem]
		return result
	
	L_dict = {}
	if k == 1:
		C = create_c1(v)
		tmp = scan_data(v, C, minSupport)
	else:
		C = create_ck(k, L)
		tmp = scan_data(v, C, minSupport)
	
	# check if L is empty then break, else update L
	if tmp != {}:
		L = tmp
	else:
		return
		
	L_dict.update(L)
	
	print(L_dict)
	return k, L_dict
	
	# for line in v.splitlines():
		# line = v.replace('-', ' ')
		# line = line.lower()
		# word_lst = line.split(" ")
		# for w in word_lst:
			# for symbol in ".,?!:;+*/()<>{}[]' ":
				# w = w.replace(symbol, '')	
			# w = w.lower()
			# if len(w) > 1:
				# yield a, w

def reducefn(k, vs):
	result = {}
	for v in vs:
		if v in result:
			result[v] = result[v] + vs[v]
		else:
			result[v] = va[v]
	return result

s = mincemeat.Server()
s.datasource = datasource
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="changeme")
print(results)

# print('The top 200 words used by Shakespeare:')
# results = sorted(results['shakespeare'].items(), key = lambda x: x[1], reverse=True)
# for i in range(200):
	# w, c = results[i]
	# print('%s\t%d' % (w, c))