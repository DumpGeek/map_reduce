#!/bin/bash

##############################################
# Script Name:	run_problem_b.sh
# Description:	a shell script to exceute problem b's code
# Arguments:	n/a
# Student ID:	1155121888
# Requirements:	python3 has to be installed into the system,
#		before execute this code
##############################################

# Exceute CMSC5741 Assignment 1 Problem B Sub-Question 1
echo ================================================
echo  CMSC5741 Assignment 1 Problem C
echo ================================================
cd ./problem_c
echo "Problem C" &&
python3 locality_sensitive_hashing.py

echo "~~~~~ End of Script ~~~~~"