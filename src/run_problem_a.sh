#!/bin/bash

##############################################
# Script Name:	run.sh
# Description:	a shell script to exceute all assignments' code
# Arguments:	n/a
# Student ID:	1155121888
# Requirements:	python3 has to be installed into the system,
#		before execute this code
##############################################


kill_server_client()
{
	echo ''
	cd ..
	echo "Terminate Server and Client ..."
	sudo pkill python3
	echo ''
}


# Ensure all python server and client have been closed
sudo pkill python3
echo ''


# Exceute CMSC5741 Assignment 1 Problem A Sub-Question 1
echo ================================================
echo  CMSC5741 Assignment 1 Problem A Sub-Question 1
echo ================================================
cd problem_a_sub_q1
echo "Server Starts Up ..." &&
python3 problem_a_sub_q1.py > result.txt & 
sleep 5 &&
echo "Client Starts Up ..." &&
echo '' &&
python3 mincemeat.py -p changeme -P 11235 127.0.0.1 &&
sleep 5 &&
cat result.txt &&
echo '' &&
read -p "Press Enter to Continue ..." INP &&
kill_server_client


# Exceute CMSC5741 Assignment 1 Problem A Sub-Question 2
echo ================================================
echo  CMSC5741 Assignment 1 Problem A Sub-Question 2
echo ================================================
cd problem_a_sub_q2
echo "Server Starts Up ..." &&
python3 problem_a_sub_q2.py > result.txt & 
sleep 5 &&
echo "Client Starts Up ..." &&
echo '' &&
python3 mincemeat.py -p changeme -P 11235 127.0.0.1 &&
sleep 5 &&
cat result.txt &&
echo '' &&
read -p "Press Enter to Continue ..." INP &&
kill_server_client


echo "~~~~~ End of Script ~~~~~"