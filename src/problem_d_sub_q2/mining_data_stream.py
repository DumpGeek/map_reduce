import time

class bucket():
	def __init__(self, size, start, end):
		self.size=size
		self.start=start
		self.end=end


class bucket_mgmt():
	def __init__(self, windowsize = 1000):
		self.bucket_list = []
		self.windowsize = windowsize
		self.frame = {}
		self.currenttime = -1
		self.timestamp = 0
		self.no_of_bit = 0
		
	def bucket_merge(self, over_freq_bucket):
		a_bucket = self.bucket_list[over_freq_bucket[1]]
		b_bucket = self.bucket_list.pop(over_freq_bucket[0])
		new_bucket = bucket(
			size = a_bucket.size + b_bucket.size, 
			start = a_bucket.start,
			end = b_bucket.end)
		self.bucket_list[over_freq_bucket[0]] = new_bucket
		return self.bucket_list
	
	def dgim(self, bit):
		# for bit in sample_data:
		# Update current time
		self.currenttime += 1
		
		# Moving Window
		self.frame[self.currenttime] =  bit
		if len(self.frame) > self.windowsize:
			del self.frame[self.currenttime - self.windowsize]
		
		# debug use
		# print("Current time:\t", self.currenttime)
		# print("Entering bit:\t", bit)
		# print("Frame:\t" + str(frame))
		# print("Frame Index:\t" + str([idx for idx, val in self.frame.items()]))
		# print("Frame Value:\t" + str("".join([val for idx, val in self.frame.items()])))
		
		# if entering bit == 1
		if bit == "1":	
			# create a new bucket to store the incoming bit
			self.bucket_list.append(bucket(1,self.currenttime, self.currenttime))
			# process to update all the buckets
			while True:
				# gather and process data from bucket list
				bucket_size_list = [buck.size for buck in self.bucket_list]
				freq_cnt = lambda x: dict((i, x.count(i)) for i in set(x))
				bucket_size_freq_dict = freq_cnt(bucket_size_list)
				# exit the loop if no more than 3 buckets with the same size
				if 3 not in bucket_size_freq_dict.values(): break
				# identify which size of the bucket has more than 3 buckets exceed 
				bucket_size_freq_3 = list(bucket_size_freq_dict.keys())[list(bucket_size_freq_dict.values()).index(3)]
				# identify index of those buckets
				over_freq_bucket = [index for index, value in enumerate(bucket_size_list) if value == bucket_size_freq_3]
				# update bucket list by merging the buckets						
				self.bucket_list = self.bucket_merge(over_freq_bucket)
			
			# debug use
			# print("currenttime - windowsize: %d" % (self.currenttime - self.windowsize))
			# for buck in self.bucket_list:
				# print("bucket\tsize: %s\t\tstart: %s\tend: %s" % (buck.size, buck.start, buck.end))
			
			# bucket size check and update
			self.no_of_bit = 0
			# if oldest bucket start exceed framewindows
			while self.bucket_list[0].end <= (self.currenttime - self.windowsize):
				# remove the bucket and add half of the bits to the 'no of bits' 
				last_buck = self.bucket_list.pop(0)
				self.no_of_bit += last_buck.size/ 2
				
				# debug use
				# print("removed bucket\tsize: %s\t\tstart: %s\tend: %s" % (last_buck.size, last_buck.start, last_buck.end))
				
			for buck in self.bucket_list:			
				self.no_of_bit += buck.size

			# print("Bucket List: ")
			# for buck in self.bucket_list:
				# print("bucket\tsize: %s\tblock: %s" % (buck.size, "".join([self.frame[idx] for idx in range(buck.end, buck.start + 1)])))
		
		# Estimation
		# print("Estimation of no of bits: \t", self.no_of_bit)
		return self.no_of_bit


if __name__ == "__main__":
	start_time = time.time()
	
	# sample_data_1 = "101011000101110110010110"
	# sample_data_2 = "0111"
	# sample_data = sample_data_1 + sample_data_2

	with open("..\..\dat\data_for_problem_d\cmsc5741_stream_data1.txt") as f: sample_data = f.read()
	
	a = bucket_mgmt(windowsize = 1000)
	for elem in sample_data:
		print("Estimation of no of bits: \t", a.dgim(elem))
		
	# Run Time
	print("=" * 20, time.time() - start_time, " seconds ","=" * 20)