from mining_data_stream import *
import os

if __name__ == "__main__":
	print(os.getcwd())
	with open("../../dat/data_for_problem_d/cmsc5741_stream_data2.txt") as f: sample_data = f.read()

	windowsize = 1000
	a = bucket_mgmt(windowsize)
	b = bucket_mgmt(windowsize)
	c = bucket_mgmt(windowsize)
	d = bucket_mgmt(windowsize)
	e = bucket_mgmt(windowsize)
	dgim_dict = {4: e, 3: d, 2: c, 1: b, 0: a}
	
	# debug use
	# bit_list = ["","","","",""]
	
	num = 0
	for elem in sample_data.strip().split(" "):
		num += 1
		binary_form = "{0:05b}".format(int(elem))
		print("[%d]\t" % num, elem, ":\t", binary_form)
		avg = 0
		for idx in range(len(binary_form)):
			bit = binary_form[len(binary_form) - idx -1]
			cnt = dgim_dict[idx].dgim(bit)
			tmp = (cnt/windowsize) * (2**idx)
			avg += tmp
			
			# debug use
			# bit_list[idx] += bit
			# print(idx, "\t", bit)
			print("idx: %d\tbit: %s\tcnt: %d\ttmp: %f\tavg: %f" %(idx, bit, cnt, tmp, avg))
			
		# Answer	
		print("average:\t", avg)
		
		# debug use
		# for elem in bit_list:
			# print(list(elem)[9000:].count("1"))
		
		print()
	
	# debug use
	# for dgim in dgim_dict.values():
		# print([val for idx, val in dgim.frame.items()].count("1"))