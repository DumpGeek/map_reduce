Shingles of each sentences:
{'ab': 1, 'ba': 1, 'ac': 1, 'cb': 1, 'bc': 1}
{'ca': 1, 'aa': 4}
{'bc': 1, 'ca': 1, 'ac': 1, 'cb': 1, 'ba': 1}
{'ca': 1, 'ab': 1, 'ba': 1, 'aa': 1, 'ac': 1}
{'ab': 1, 'ba': 1, 'ac': 1, 'ca': 1, 'aa': 1}
==================================================
Sorted List:
 ['aa', 'ab', 'ac', 'ba', 'bc', 'ca', 'cb']

Enum List:
 [(0, 'aa'), (1, 'ab'), (2, 'ac'), (3, 'ba'), (4, 'bc'), (5, 'ca'), (6, 'cb')]
==================================================
Question A Answer:
0 aa [0, 1, 0, 1, 1]
1 ab [1, 0, 0, 1, 1]
2 ac [1, 0, 1, 1, 1]
3 ba [1, 0, 1, 1, 1]
4 bc [1, 0, 1, 0, 0]
5 ca [0, 1, 1, 1, 1]
6 cb [1, 0, 1, 0, 0]
==================================================
permutation 1:  [1, 3, 5, 7, 9, 0, 2]
permutation 2:  [2, 5, 8, 1, 4, 7, 0]
permutation 3:  [2, 1, 0, 5, 4, 3, 2]

signature matrix 1: [2, 0, 0, 0, 0]
signature matrix 2: [0, 2, 0, 1, 1]
signature matrix 3: [0, 2, 0, 0, 0]
==================================================
Combination list:  [[0, 1], [0, 2], [0, 3], [0, 4], [1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4]]

 0-1
True Jaccard Similarity:	 0.0
Estimated Jaccard Similarity:	 0.0

 0-2
True Jaccard Similarity:	 0.6666666666666666
Estimated Jaccard Similarity:	 0.6666666666666666

 0-3
True Jaccard Similarity:	 0.42857142857142855
Estimated Jaccard Similarity:	 0.3333333333333333

 0-4
True Jaccard Similarity:	 0.42857142857142855
Estimated Jaccard Similarity:	 0.3333333333333333

 1-2
True Jaccard Similarity:	 0.16666666666666666
Estimated Jaccard Similarity:	 0.3333333333333333

 1-3
True Jaccard Similarity:	 0.4
Estimated Jaccard Similarity:	 0.3333333333333333

 1-4
True Jaccard Similarity:	 0.4
Estimated Jaccard Similarity:	 0.3333333333333333

 2-3
True Jaccard Similarity:	 0.42857142857142855
Estimated Jaccard Similarity:	 0.6666666666666666

 2-4
True Jaccard Similarity:	 0.42857142857142855
Estimated Jaccard Similarity:	 0.6666666666666666

 3-4
True Jaccard Similarity:	 1.0
Estimated Jaccard Similarity:	 1.0
