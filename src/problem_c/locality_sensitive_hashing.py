''''
Problem C. Locality Sensitive Hashing (20 points)

The following problem is based on lecture 3.

This problem is related to the concept about shingling, minhashing, and locality sensitive 

hashing. Suppose you are given the following five sentences:

I. abacbc

II. caaaaa

III. bcacba

IV. cabaac

V. abacaa

a. Calculate the set of 2-shingles for each sentence and use matrix to represent the 

sentences, where each column denotes one sentence and rows denote elements 

(shingles) in these sentences. The element is enumerated from 0 in alphabetical order. 

b. Use matrix to represent the minhash signature for each sentence if we use the following 

three hash functions: ℎ1

(𝑥) = 2𝑥 + 1 mod 11; ℎ2

(𝑥) = 3𝑥 + 2 mod 10; ℎ3

(𝑥) = 5𝑥 +

2 mod 6. Each column denotes one sentence and rows denote signatures in ℎ1 - ℎ2 - ℎ3

order.

c. Which of these hash functions are true permutations?

d. Calculate the estimated Jaccard similarities and true Jaccard similarities for all pairs of 

columns.

For this problem, please write your answers in the word file.
'''

def shingles(sentence, k):
	shingles_dict={}
	for i in range(0, len(sentence)-k+1):
		shingle=""
		for j in range(0, k):
			shingle+=sentence[i+j]
		if shingle not in shingles_dict:
			shingles_dict[shingle] = 1
		else:
			shingles_dict[shingle] += 1
	return shingles_dict

dataset = "abacbc\ncaaaaa\nbcacba\ncabaac\nabacaa"
sentences = dataset.splitlines()

shingles_list = []

print("Shingles of each sentences:")
for i in range(len(sentences)):
	shingles_dict = shingles(sentences[i],2)
	print(shingles_dict)
	shingles_list.append(shingles_dict)

print('='*50)

a_set = set()
for elem in shingles_list:
	a_set = a_set.union(set(elem))

a_set = list(a_set)
a_set.sort()
print("Sorted List:\n", a_set)

print()

enum_list = list(enumerate(a_set))
print("Enum List:\n", enum_list)

print('='*50)


print('Question A Answer:')
enum_dict_table = {}
for enum in enum_list:
	shingle_row = []
	for shingle in shingles_list:
		idx, val = enum
		if val in shingle:
			shingle_row.append(1)
		else:
			shingle_row.append(0)
	enum_dict_table[enum[0]] = {'shingle':enum[1],'row':shingle_row}

for enum in enum_dict_table:
	print(enum, enum_dict_table[enum]['shingle'],enum_dict_table[enum]['row'])
	
print('='*50)

hash_1 = lambda x: (2*x+1) % 11
hash_2 = lambda x: (3*x+2) % 10
hash_3 = lambda x: (5*x+2) % 6

permutation_1 = list(map(hash_1, range(len(enum_dict_table))))
permutation_2 = list(map(hash_2, range(len(enum_dict_table))))
permutation_3 = list(map(hash_3,  range(len(enum_dict_table))))

p_list = [permutation_1, permutation_2, permutation_3]

cnt = 1
for p in p_list:
	print('permutation %s: ' % cnt, p)
	cnt += 1

print('')

signature_matrix_list = []
for p in p_list:
	signature_matrix = []
	
	for j in range(len(sentences)):
		for i in range(len(enum_dict_table)):
			if enum_dict_table[p.index(i)]['row'][j] == 1:
				signature_matrix.append(i)
				break
	signature_matrix_list.append(signature_matrix)
	
cnt = 1
for s in signature_matrix_list:
	print('signature matrix %s:' % cnt, s)
	cnt += 1
	
print('='*50)

combination_lst = []
for i in range(len(sentences)):
	for j in range(len(sentences)):
		if i != j and ( [i,j] not in combination_lst and [j,i] not in combination_lst ):
			combination_lst.append([i,j])
			
print("Combination list: ", combination_lst)

a_union_b = lambda a,b: [x and y for x, y in zip(a, b)]
a_intersect_b = lambda a,b: [x or y for x, y in zip(a, b)]
ab_similarity = lambda a,b: sum(a_union_b(a,b))/sum(a_intersect_b(a,b))

c_union_d = lambda c,d: [x == y for x, y in zip(c,d)]
cd_similarity = lambda c,d: sum(c_union_d(c,d))/len(c)

true_jaccard_similarity={}
estimate_jaccard_similarity={}

for i,j in combination_lst:
	#print('\n', i, ' ,',j ,' :')
	a_lst = []
	b_lst = []
	for y in range(len(enum_dict_table)):
		a_lst.append(enum_dict_table[y]['row'][i])
		b_lst.append(enum_dict_table[y]['row'][j])
	# print('a_lst:\t', a_lst)
	# print('b_lst:\t', b_lst)
	# print('and:\t', a_union_b(a_lst, b_lst))
	# print('or:\t', a_intersect_b(a_lst,b_lst))
	# print('similarity:\t', ab_similarity(a_lst, b_lst))
	true_jaccard_similarity['%d-%d'%(i,j)] = ab_similarity(a_lst, b_lst)
	
	c_lst = []
	d_lst = []
	for y in range(len(signature_matrix_list)):
		c_lst.append(signature_matrix_list[y][i])
		d_lst.append(signature_matrix_list[y][j])
	# print('c_lst:\t', c_lst)
	# print('d_lst:\t', d_lst)
	# print('and:\t', c_union_d(c_lst, d_lst))
	# print('similarity:\t', cd_similarity(c_lst, d_lst))
	estimate_jaccard_similarity['%d-%d'%(i,j)] = cd_similarity(c_lst, d_lst)

for elem in true_jaccard_similarity:
	print('\n', elem)
	print('True Jaccard Similarity:\t', true_jaccard_similarity[elem])
	print('Estimated Jaccard Similarity:\t', estimate_jaccard_similarity[elem])

