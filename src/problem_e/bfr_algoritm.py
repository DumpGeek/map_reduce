fn_n = lambda x: (len(x))
fn_sum = lambda x: (sum([a for a, b in x]), sum([b for a, b in x]))
fn_sumq = lambda x: (sum([a**2 for a, b in x]), sum([b**2 for a, b in x]))

data_pts = [
	(10,4), (11,5), (10,6), (9,6), (6,2),
	(6,4), (7,2), (5,4), (6,10), (5,10), 
	(5,12), (1,7), (1,8), (2,9), (2,9) 
	]
    
m = 4
clusters = [data_pts[i:i+m] for i in range(0,len(data_pts),m)]

cnt = 0
for cluster in clusters:
		n = fn_n(cluster)
		s = fn_sum(cluster)
		q = fn_sumq(cluster)
		print("Cluster %d: %s\t\tN: %d\tSUM: %s\tSUMQ: \t%s" %(cnt, str(cluster), n, str(s), str(q)))
		cnt += 1