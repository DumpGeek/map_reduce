distance = lambda x1,y1,x2,y2: ((x1-x2)**2 + (y1-y2)**2)**0.5
new_centroid = lambda x: (sum([a for a,b in x ])/len(x), sum([b for a,b in x ])/len(x))

data_pts = [
	(10,4), (11,5), (10,6), (9,6), (6,2),
	(6,4), (7,2), (5,3), (6,10), (5,10), 
	(5,12), (1,7), (1,8), (2,9), (2,9) 
	]

print("Data points:")
for i in range(len(data_pts)):
	print("A%d:\t%s" %(i+1, str(data_pts[i])))
print()


centroids = [data_pts[idx] for idx in [0,4,8,11]]


cnt = 0
mirror = []
while True:
    cnt += 1    
    cluster_a, cluster_b, cluster_c, cluster_d = [], [], [], []
    cluster = [cluster_a, cluster_b, cluster_c, cluster_d]
        
    # debug use
    print("~~~~~ Iteration %d ~~~~~" % cnt)
    print("Centroids: ", centroids)
    
    for b in data_pts:
        tmp = []
        for i in range(len(centroids)):
            a = centroids[i]
            tmp.append(distance(*a, *b))
        
        idx = tmp.index(min(tmp))
        cluster[idx].append(b)
        
    for i in range(len(cluster)):
    	centroids[i] = new_centroid(cluster[i]) 
     
    
    # debug use
    print("Distances: ", tmp)
    for idx, elem in enumerate(cluster):
        print("Cluster %d: " % idx, elem)
    print("New centroids: ", centroids)

     
    if mirror == cluster:
        print("\n")
        print("No difference between Iteration %d and Iteration %d" % (cnt, cnt-1))
        print("No more iteration is needed")
        print("~~~~~ End ~~~~~")
        break
    else:
        mirror = cluster.copy()
    
    # Seperation line
    print()
    #input()