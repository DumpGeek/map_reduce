"""
A useful model of stream processing is that queries are about a window of length N. Usually, we 

use DGIM algorithm to count ones in a bit stream. You are required to implement the 

algorithm in O(log2 𝑁) space complexity with O(log log 𝑁) bucket size.

1. In Amazon, for every product X we keep 0/1 stream of whether that product was sold in 

the n-th transaction. We want to answer queries, how many times have we sold X in the last 

K sales. Supposed that we have ten-thousand records (provided in 

cmsc5741_stream_data1.txt) of product X in Amazon. You are required to program to 

complete the query in last one thousand sales through DGIM algorithm. Output should 

demonstrate how you set the buckets and your estimate. 

Updating rules: 

(1) If the current bit is 0, no other changes are needed; 

(2) If the current bit is 1:

	(a) Create a new bucket of size 1, for just this bit, and end timestamp = current time;

	(b) If there are now three buckets of size 1, combine the oldest two into a bucket of size 2;

	(c) If there are now three buckets of size 2, combine the oldest two into a bucket of size 
4;

	(d) And analogize for the rest.
"""
import time
start_time = time.time()

class bucket():
	def __init__(self, size, start, end):
		self.size=size
		self.start=start
		self.end=end

def bucket_merge(bucket_list, over_freq_bucket):
	a_bucket = bucket_list[over_freq_bucket[1]]
	b_bucket = bucket_list.pop(over_freq_bucket[0])
	new_bucket = bucket(
		size = a_bucket.size + b_bucket.size, 
		start = a_bucket.start,
		end = b_bucket.end)
	bucket_list[over_freq_bucket[0]] = new_bucket
	return bucket_list
	
	
bucket_list = []
windowsize = 1000
frame = {}
currenttime = -1
timestamp = 0

# sample_data_1 = "101011000101110110010110"
# sample_data_2 = "0111"
# sample_data = sample_data_1 + sample_data_2

with open("../../dat/data_for_problem_d/cmsc5741_stream_data1.txt") as f: sample_data = f.read()


for bit in sample_data:
	# Update current time
	currenttime += 1
	
	# Moving Window
	frame[currenttime] =  bit
	if len(frame) > windowsize:
		del frame[currenttime - windowsize]
	
	# debug use
	# print("Current time:\t", currenttime)
	# print("Frame:\t" + str(frame))
	# print("Frame Index:\t" + str([idx for idx, val in frame.items()]))
	# print("Frame Value:\t" + str("".join([val for idx, val in frame.items()])))
	
	# if entering bit == 1
	if bit == "1":	
		# create a new bucket to store the incoming bit
		bucket_list.append(bucket(1,currenttime, currenttime))
		# process to update all the buckets
		while True:
			# gather and process data from bucket list
			bucket_size_list = [buck.size for buck in bucket_list]
			freq_cnt = lambda x: dict((i, x.count(i)) for i in set(x))
			bucket_size_freq_dict = freq_cnt(bucket_size_list)
			# exit the loop if no more than 3 buckets with the same size
			if 3 not in bucket_size_freq_dict.values(): break
			# identify which size of the bucket has more than 3 buckets exceed 
			bucket_size_freq_3 = list(bucket_size_freq_dict.keys())[list(bucket_size_freq_dict.values()).index(3)]
			# identify index of those buckets
			over_freq_bucket = [index for index, value in enumerate(bucket_size_list) if value == bucket_size_freq_3]
			# update bucket list by merging the buckets						
			bucket_list = bucket_merge(bucket_list, over_freq_bucket)
			
			# debug use:
			#print("bucket_size_list:\t" + str(bucket_size_list))
			#print("bucket_size_freq_dict:\t" + str(bucket_size_freq_dict)) 
			#print(list(bucket_size_freq_dict.values()).index(3))
			#print("bucket_size_freq_3:\t" + str(bucket_size_freq_3))						
			#print("over_freq_bucket" + str(over_freq_bucket))
			#print("bucket_list:\t" + str(bucket_list))	
						
		# debug use
		# print("currenttime - windowsize: %d" % (currenttime - windowsize))
		# for buck in bucket_list:
			# print("bucket\tsize: %s\t\tstart: %s\tend: %s" % (buck.size, buck.start, buck.end))
		
		# bucket size check and update
		no_of_bit = 0
		# if oldest bucket start exceed framewindows
		while bucket_list[0].end <= (currenttime - windowsize):
			# remove the bucket and add half of the bits to the 'no of bits' 
			last_buck = bucket_list.pop(0)
			no_of_bit += last_buck.size/ 2
			
			# debug use
			# print("removed bucket\tsize: %s\t\tstart: %s\tend: %s" % (last_buck.size, last_buck.start, last_buck.end))
				
		# data visualization
		# debug use
		# print("Bucket List: ", end = "")
		
		for buck in bucket_list:			
			no_of_bit += buck.size
		
			# debug use
			# print("bucket\tsize: %s\t\tstart: %s\tend: %s\nblock: %s" % (buck.size, buck.start, buck.end, "".join([frame[idx] for idx in range(buck.end, buck.start + 1)])))
			# print(["".join([frame[idx] for idx in range(buck.end, buck.start + 1)])], end = "")
		# print()
		# print("No of bits: \t", no_of_bit)
	
	# Spacing
	# print("\n")

# Answer
# Bucket Arrangement
print("Bucket List: ")
for buck in bucket_list:
	print("bucket\tsize: %s\tblock: %s" % (buck.size, "".join([frame[idx] for idx in range(buck.end, buck.start + 1)])))			
	#print(["".join([frame[idx] for idx in range(buck.end, buck.start + 1)])], end = "")
# Estimation
print("\nEstimation of no of bits: \t", no_of_bit, "\n")
# Run Time
print("=" * 20, time.time() - start_time, " seconds ","=" * 20)