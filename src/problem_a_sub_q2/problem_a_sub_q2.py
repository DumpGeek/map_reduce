#!/usr/bin/env python
# -*- coding: utf-8 -*

import mincemeat
import stopwords
import glob

__version__ = '0.0.0'	# '{major}.{minor}.{patch}'

# shakespeare text file relative path
text_files = ["../../dat/data_for_problem_a/shakespeare.txt"]

def file_contents(file_name):
	# modified: specify using utf-8 as encoding to avoid error
	f = open(file_name, encoding="utf8")
	try:
		return f.read()
	finally:
		f.close()

# The data source can be any dictionary-like object
datasource, cnt = {}, 0
for line in file_contents(text_files[0]).splitlines():
	datasource.update({cnt: line})
	cnt += 1

def mapfn(k, v):
	a = "shakespeare"
	line = v.replace('-', ' ')
	line = line.lower()
	word_lst = line.split(" ")
	for w in word_lst:
		for symbol in ".,?!:;+*/()<>{}[]' ":
			w = w.replace(symbol, '')	
		w = w.lower()
		if len(w) > 1 and w not in stopwords.allStopWords:
			yield a, w
		
def reducefn(k, vs):
	result = {}
	for v in vs:
		if v in result:
			result[v] = result[v] + 1
		else:
			result[v] = 1
	return result

s = mincemeat.Server()
s.datasource = datasource
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="changeme")

print('The top 200 words used by Shakespeare excluding words in the lists:')
results = sorted(results['shakespeare'].items(), key = lambda x: x[1], reverse=True)
for i in range(200):
	w, c = results[i]
	print('%s\t%d' % (w, c))