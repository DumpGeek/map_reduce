# stop words list 1
with open("../../dat/data_for_problem_a/stopwords1.txt") as f:
	stopwords_1 = f.read().splitlines()

# stop words list 2
with open("../../dat/data_for_problem_a/stopwords2.txt") as f:
	stopwords_2 = f.read().splitlines()
	
# List of all stop words
allStopWords = stopwords_1 + stopwords_2