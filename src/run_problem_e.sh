#!/bin/bash

##############################################
# Script Name:	run_problem_e.sh
# Description:	a shell script to exceute problem b's code
# Arguments:	n/a
# Student ID:	1155121888
# Requirements:	python3 has to be installed into the system,
#		before execute this code
##############################################

# Exceute CMSC5741 Assignment 1 Problem E Sub-Question 1
echo ================================================
echo  CMSC5741 Assignment 1 Problem E Sub-Question 1
echo ================================================
cd ./problem_e
echo "Question 1" &&
python3 euclidean.py

echo ''
read -p "Press Enter to Continue ..." INP &&

# Exceute CMSC5741 Assignment 1 Problem E Sub-Question 2
echo ================================================
echo  CMSC5741 Assignment 1 Problem E Sub-Question 2
echo ================================================
echo "Question 2" &&
python3 bfr_algoritm.py

echo "~~~~~ End of Script ~~~~~"