from os import path
try: 
	from setuptools import setup 
except ImportError: 
	from distutils.core import setup 
 
 here = path.abspath(path.dirname(__file__))
 
 with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()
 
 with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
	requirements = f.read().splitlines()
#requirements.append("nose")
 
config = { 
	"description": "My Project", 
	"author": "My Name", 
	"url": "URL to get it at.", 
	"download_url": "Where to download it.", 
	"author_email": "My email.", 
	"version": "0.0.0", 
	"install_requires": requirements, 
	"packages": ["demo"], 
	"scripts": [], 
	"name": "projectname" 
} 
 
setup(**config)
