#!interpreter [optional-arg]	#!/usr/bin/env python
# -*- coding: utf-8 -*-			# -*- coding: utf-8 -*

'''
{Description}
{License_info}

Good practices for any scripting language
------------------------------------------
It contained a few common ideas as:
	Interpreter
	Encoding
	Basic description
	Current version
	Imports
	Date of creation
	…and for some reason…
	Complete update history (I’m sorry, don’t be like me)
'''

# Futures
from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
# […]

# Libs
import pandas as pd # Or any other
# […]

# Own modules
from {path} import {class}
# […]

__author__ = '{author}'
__copyright__ = 'Copyright {year}, {project_name}'
__credits__ = ['{credit_list}']
__license__ = '{license}'
__version__ = '{major}.{minor}.{patch}'
__maintainer__ = '{maintainer}'
__email__ = '{contact_email}'
__status__ = '{dev_status}'